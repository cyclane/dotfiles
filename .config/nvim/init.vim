" Default sets
set mouse=a
set showcmd
set number
set relativenumber
set nowrap
set tabstop=4
set shiftwidth=4
set encoding=UTF-8
set clipboard+=unnamedplus
set spell spelllang=en_gb

let mapleader = " "

" Plugs
call plug#begin(stdpath('data') . '/plugged')

Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'rmagatti/auto-session'

Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'
Plug 'ctrlpvim/ctrlp.vim' " fuzzy files

Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdcommenter'

Plug 'editorconfig/editorconfig-vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'gcmt/taboo.vim'

" language specific
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'jparise/vim-graphql'

call plug#end()

" Maps
"" File
map <leader>fs :w<CR>
"" Quit
map <leader>qq :qa<CR>
"" Window mode
map <leader>w <C-w>
"" Toggles
map <leader>tn :NERDTreeToggle<CR>
map <C-_> <plug>NERDCommenterToggle
map <leader>tw :set wrap!<CR>
map <leader>tc <plug>NERDCommenterToggle
"" LSP
map <leader>lh :lua vim.lsp.buf.hover()<CR>
map <C-K> :lua vim.lsp.buf.hover()<CR>
map <leader>lD :lua vim.lsp.buf.declaration()<CR>
map <leader>ld :lua vim.lsp.buf.definition()<CR>
map <leader>li :lua vim.lsp.buf.implementation()<CR>
map <leader>lt :lua vim.lsp.buf.type_definition()<CR>
map <leader>lr :lua vim.lsp.buf.rename()<CR>
map <leader>la :lua vim.lsp.buf.code_action()<CR>
map <leader>lf :lua vim.lsp.buf.formatting()<CR>
map <leader>le :lua vim.lsp.diagnostic.show_line_diagnostics()<CR>
"" Buffer
map <leader>bd :bd<CR>
map <leader>bD :bd!<CR>
map <leader>bn :bn<CR>
map <leader>bp :bp<CR>
"" Golang
map <leader>gb :GoDebugBreakpoint<CR>
map <leader>gc :GoDebugContinue<CR>
map <leader>gs :GoDebugStep<CR>
map <leader>gS :GoDebugStepOut<CR>
map <leader>gn :GoDebugNext<CR>
map <leader>gd :GoDebugStart<CR>
map <leader>gD :GoDebugStop<CR>
"" Files
map <leader><leader> :CtrlP<CR>



" NERDCommenter
let g:NERDSpaceDelims = 1
let g:NERDToggleCheckAllLines = 1

" airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ""
let g:airline#extensions#tabline#right_sep = ""

let g:airline_left_sep = ""
let g:airline_right_sep = ""

" theming
if has('termguicolors')
		set termguicolors
endif
let g:sonokai_style = 'andromeda'
let g:sonokai_enable_italic = 1
let g:airline_theme = 'sonokai'

colorscheme sonokai

" LSP


" Autocompletion
lua << EOF
local servers = { 'gopls', 'bashls', 'pyright', 'texlab', 'rust_analyzer', 'graphql' }

local nvim_lsp = require('lspconfig')
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = require'completion'.on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }
end
EOF
autocmd BufEnter * lua require'completion'.on_attach()
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
set completeopt=menuone,noinsert,noselect
set shortmess+=c

" Golang
let g:go_debug_windows = {
			\ 'vars':       'leftabove 30vnew',
			\ 'stack':	    'rightbelow 30new',
			\ 'goroutines': 'botright 10new',
			\ 'out':        'leftabove vnew',
\ }
