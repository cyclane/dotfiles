#!/bin/bash
nvim_sonokai=~/.config/nvim/autoload/sonokai.vim

# Swap sonokai blue with orange because it's nice
cp ~/.remote/sonokai/autoload/sonokai.vim $nvim_sonokai
sed -i "s/'orange'/'_oldorg'/" $nvim_sonokai
sed -i "s/'blue'/'orange'/" $nvim_sonokai
sed -i "s/'_oldorg'/'blue'/" $nvim_sonokai
sed -i "s/77d5f0/f89860/" $nvim_sonokai
