# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export GOPATH=$HOME/go
export PATH=$HOME/.local/bin:$GOPATH/bin:$PATH
export OPENCV_LOG_LEVEL=ERROR # Howdy warnings hack
export EDITOR=nvim
export ZSH=$HOME/.remote/ohmyzsh

ZSH_THEME="powerlevel10k/powerlevel10k"
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="yyyy-mm-dd"
ZSH_CUSTOM=$HOME/.config/ohmyzsh
plugins=(git fast-syntax-highlighting)

alias vim=nvim # lol
alias powernow=power_now
alias pn=power_now
alias batteryinfo=battery_info
alias bi=battery_info
alias rename=perl-rename
alias ssh="TERM=xterm-256color $(which ssh)"

source $ZSH/oh-my-zsh.sh

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
