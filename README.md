# My GNU/Linux dotfiles

Not intended to work well, but well enough for me.

![Screenshot 1](./.screenshots/1.png)
![Screenshot 2](./.screenshots/2.png)

----

### Display Manager
- [lxdm](https://wiki.lxde.org/en/LXDM)

### Window Manager & Desktop
- [bspwm](https://github.com/baskerville/bspwm)
- [sxhkd](https://github.com/baskerville/sxhkd)
- [polybar](https://github.com/polybar/polybar)
- [rofi](https://github.com/davatorium/rofi)
- [mpd](https://www.musicpd.org/)
- [flameshot](https://github.com/flameshot-org/flameshot)
- [betterlockscreen](https://github.com/betterlockscreen/betterlockscreen)
- [dunst](https://github.com/dunst-project/dunst)

### Terminal
- [kitty](https://github.com/kovidgoyal/kitty)
- [neovim](https://github.com/neovim/neovim)
- [zsh](https://www.zsh.org/)

### Fonts
- [FiraCode Nerd Font](https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/FiraCode/Regular/complete/Fira%20Code%20Regular%20Nerd%20Font%20Complete.ttf)

### Other
- [redshift](https://github.com/jonls/redshift)
- [picom](https://github.com/tryone144/picom/tree/feature/dual_kawase) (tryone's dual_kawase fork)

----

## Hostname files
Some files will have something along the lines of

```sh
hostname=$(cat /etc/hostname)
// ...
if [[ $hostname == "something" ]]; then
	// ...
fi
```

Here is a list of the files that are hostname-dependant
- .local/bin/touch_calibrate
- .config/bspwm/bspwmrc
- .xprofile

## Sources

Some configs - especially rofi menus, are modified versions or inspired by an existing config.
Things not listed here probably have no external base / inspiration; however, it
could also just be that I forgot to add it.

### Colours

Most colours used are taken directly from [sonokai](https://github.com/sainnhe/sonokai)
andromeda theme. Some have darker or lighter variations of colours from that theme.

These colours apply pretty much everywhere in places such as the polybar(s),
rofi menus, zsh theme, kitty (terminal emulator) colours e.t.c.

### Rofi Menu

The rofi menu is a modified version of the rofi menu
from the default [Archcraft](https://archcraft.io/) theme.

### Polybar

The polybar(s) are inspired by the [Archcraft](https://archcraft.io/)
default polybar theme.

Main changes are the general layout, and some `format-*` config values.

### NeoVIM

Uses the [sonokai](https://github.com/sainnhe/sonokai) andromeda theme
with a slight change in the vim-airline theme to make orange the
dominant colour.

### ZSH & Kittty

Prompt created with `p10k configure` and colours slightly modified.


## Installation

No full instructions for now.
Cloning and moving all the files into your home directory should get most
software working.

### .install.sh

This is not an installer file, rather an updater file for modified files
that are based on remotes. It is mostly used for modifying the sonokai colours
to make orange the dominant colour in different places.
